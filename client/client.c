#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdbool.h>

#define SERVER_IP "127.0.0.1"
#define PORT 8080

bool isRoot = false;

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Invalid command.\n");
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Invalid command.\n");
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *username = argv[2];
    char *password = argv[4];

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[5000]= {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Socket creation error\n");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, SERVER_IP, &serv_addr.sin_addr) <= 0) {
        printf("Invalid address/ Address not supported\n");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("Connection Failed 47\n");
        exit(EXIT_FAILURE);
    }

    if (geteuid() == 0) {
        isRoot = true;
    }
    char authenticate[100];
    if(isRoot){
        sprintf(authenticate, "launcher root 0");
    }else{
        sprintf(authenticate, "launcher %s %s", username, password);
    }
    

    printf("Launcher command: %s\n", authenticate);
    send(sock, authenticate, strlen(authenticate), 0);

    memset(buffer, 0, sizeof(buffer));
    valread = recv(sock, buffer, sizeof(buffer), 0);
    printf("Response from server: %s\n", buffer);

    char cmd[1024];

    while(1){
        fgets(cmd, sizeof(cmd), stdin);
        printf("Client cmd: %s\n", cmd);
        send(sock, cmd, strlen(cmd), 0);

        //wait();

        memset(buffer, 0, sizeof(buffer));
        valread = recv(sock, buffer, sizeof(buffer), 0);
        printf("Response from server: %s\n\n", buffer);
    }

    close(sock);

    return 0;
}
