# sisop-praktikum-fp-2023-AM-B04

## Anggota Kelompok B04:
+ Akbar Putra Asenti Priyanto (5025211004)
+ Muhammad Rafi Sutrisno (5025211167)
+ Muhammad Rafi Insan Fillah (5025211169)

## client.c
```
int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Invalid command.\n");
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printf("Invalid command.\n");
        printf("Usage: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *username = argv[2];
    char *password = argv[4];
```

Program client disini memiliki format command berupa `./[program_client] -u [username] -p [password]`. Di awal command juga dapat ditambahkan command sudo agar dapat login sebagai root user, yaitu `sudo ./[program_client] -u [username] -p [password]`. Selain kedua format ini, program client tidak dapat berjalan.

Setelah pengecekan format, argv[2] akan dimasukkan ke dalam string username, dan argv[4] akan dimasukkan ke dalam string password untuk dilakukan pengecekan selanjutnya.
```
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[5000]= {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Socket creation error\n");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, SERVER_IP, &serv_addr.sin_addr) <= 0) {
        printf("Invalid address/ Address not supported\n");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("Connection Failed 47\n");
        exit(EXIT_FAILURE);
    }
```

Baris ini digunakan untuk menghubungkan program client ke socket database, untuk nilai default portnya diset sebagai port 8080.
```
    if (geteuid() == 0) {
        isRoot = true;
    }
    char authenticate[100];
    if(isRoot){
        sprintf(authenticate, "launcher root 0");
    }else{
        sprintf(authenticate, "launcher %s %s", username, password);
    }
    
    printf("Launcher command: %s\n", authenticate);
    send(sock, authenticate, strlen(authenticate), 0);

    memset(buffer, 0, sizeof(buffer));
    valread = recv(sock, buffer, sizeof(buffer), 0);
    printf("Response from server: %s\n", buffer);

    char cmd[1024];
```

Selanjutnya akan dilakukan pengiriman username dan password ke socket agar dapat diautentikasi oleh program database. Pertama, akan dilakukan pengecekan apakah program client dijalankan oleh root atau bukan. Jika iya, maka username yang dikirimkan adalah "root" dengan password "0" (karena root tidak memerlukan password yang perlu dicek). Jika tidak, maka yang dikirimkan ke socket adalah username dan password yang diinput pada awal running program.

Setelah mengirim username dan password, akan dilakukan recv() dari socket untuk mengecek hasil autentikasi dari program database untuk mengecek apakah username dan password telah sesuai atau belum.
```
    char cmd[1024];
    while(1){
        fgets(cmd, sizeof(cmd), stdin);
        printf("Client cmd: %s\n", cmd);
        send(sock, cmd, strlen(cmd), 0);

        //wait();

        memset(buffer, 0, sizeof(buffer));
        valread = recv(sock, buffer, sizeof(buffer), 0);
        printf("Response from server: %s\n\n", buffer);
    }

    close(sock);

    return 0;
}
```

Jika username dan password sudah sesuai, maka akan dilakukan while true looping yang diawali dengan fungsi fgets() untuk menerima input dari user melalui stdin dan dimasukkan ke dalam string cmd. String cmd tersebut kemudian akan dikirim ke socket untuk diproses oleh program database. Program database kemudian akan mengirim pesan ke program client, sehingga untuk membacanya perlu dilakukan fungsi recv() dari socket. 


## database.c

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

#define PORT 8080
#define MAX_USERNAME_LENGTH 20
#define MAX_PASSWORD_LENGTH 20
#define DATABASE_DIR "./database/databases"
#define LOG_DIR "./database/dblog.txt"

#define MAX_LINE_LENGTH 1000
#define MAX_COLUMNS 10

char CURDATABASE[1024]="";

#define ACCESS_DB_DIR "./database/databases/access_database"
```

Pertama include library dan pendefinisian variabel absolut dan variabel global yang akan dipakai.

## fungsi-fungsi pembantu

```
void createUser(char *username, char *password) {
    char filePath[100];
    sprintf(filePath, "%s/users.csv", ACCESS_DB_DIR);

    FILE *file = fopen(filePath, "a");
    if (file != NULL) {
        fprintf(file, "%s,%s\n", username, password);
        fclose(file);
        printf("User created: %s\n", username);
    } else {
        perror("Failed to create user");
    }
}

```

Fungsi createUser adalah fungsi dengan return void untuk menambah line username dan password kedalam access_database/users.csv. createUser membuka file csv users.csv dengan mode append dan append new line username dan password yang akan didaftarkan.

```
int grantAccess(char *username, char *databaseName) {
    char filePath[100];
    sprintf(filePath, "%s/access.csv", ACCESS_DB_DIR);

    FILE *file = fopen(filePath, "a");
    if (file != NULL) {
        fprintf(file, "%s,%s\n", username, databaseName);
        fclose(file);
        printf("Access granted: %s -> %s\n", username, databaseName);
        return 0;
    } else {
        printf("Failed to Grant access");
        return 1;        
    }
}
```

Fungsi grantAccess adalah fungsi dengan return int untuk memberi akses baru dengan mendaftarkan username yang ingin didaftarkan terhadap sebuah nama database kedalam access_database/access.csv. grantAccess membuka file csv dengan moded append dan append username dan nama database yang ingin didaftarkan, jika append berhasil akan return 0 dan jika gagal return 1.

```
int isUsernameExists(char *username) {
    char filePath[100];
    sprintf(filePath, "%s/users.csv", ACCESS_DB_DIR);

    FILE *file = fopen(filePath, "r");
    if (file != NULL) {
        char line[256];
        char loadedUsername[MAX_USERNAME_LENGTH];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,]", loadedUsername);

            if (strcmp(loadedUsername, username) == 0) {
                fclose(file);
                return 1; // Username exists in the users table
            }
        }

        fclose(file);
    }

    return 0; // Username does not exist in the users table
}
```

Fungsi isUsernameExist adalah fungsi dengan return int untuk mengecek apakah string username sudah pernah didaftarkan pada access_database/users.csv, jika ada maka akan return 1 dan jika tidak return 0.

```
void rmAccDropDB(char *database){
    char filename[100];
    sprintf(filename, "%s/access.csv", ACCESS_DB_DIR);

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Failed to open file '%s'.\n", filename);
        return;
    }

    FILE* tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL) {
        printf("Failed to create temporary file.\n");
        fclose(file);
        return;
    }

    char line[1024];
    while (fgets(line, sizeof(line), file) != NULL) {
        char loadedUsername[1024];
        char loadedDatabase[1024];
        sscanf(line, "%[^,],%s", loadedUsername, loadedDatabase);

        if (strcmp(loadedDatabase, database) != 0) {
            fputs(line, tempFile);
        }
    }

    fclose(file);
    fclose(tempFile);

    // Remove the original file
    if (remove(filename) != 0) {
        printf("Failed to remove file '%s'.\n", filename);
        return;
    }

    // Rename the temporary file to the original file name
    if (rename("temp.csv", filename) != 0) {
        printf("Failed to rename temporary file.\n");
        return;
    }

    printf("Rows with the second column '%s' have been removed from '%s'.\n", database, filename);
}
```

Fungsi rmAccDropDB merupakan void function untuk menghapus setiap line pada access_database/access.csv jika pada line tersebut terdapat nama database yang diterima pada parameter. Fungsi ini dipanggil sesaat setelah drop database dengan maksud menghapus access database berkaitan yang telah didrop.

```
void removeColumnFromCSV(const char* filename, const char* attrName) {
    // Open the input file for reading
    char filepath[3072];
    sprintf(filepath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, filename);
    //printf("read csv: %s\n", filepath);

    FILE* input = fopen(filepath, "r");
    if (input == NULL) {
        perror("Error opening input file");
        return;
    }

    // Open a temporary output file for writing
    char temppath[3072];
    sprintf(temppath, "%s/%s/temp_%s.csv", DATABASE_DIR, CURDATABASE, filename);
    //printf("temp csv: %s\n", temppath);

    FILE* output = fopen(temppath, "w");
    if (output == NULL) {
        perror("Error opening temporary output file");
        fclose(input);
        return;
    }

    // Read the first row (attribute names)
    char row[1024];
    if (fgets(row, sizeof(row), input) == NULL) {
        perror("Error reading input file");
        fclose(input);
        fclose(output);
        return;
    }

    // Find the index of the column to remove
    int index = -1;
    int colIndex = 0;
    char* token = strtok(row, ",");
    while (token != NULL) {
        size_t tokenLen = strlen(token);
        if (token[tokenLen - 1] == '\n') {
            token[tokenLen - 1] = '\0';
        }
        //printf("compare token: %s == attrName: %s\n", token, attrName);
        if (strcmp(token, attrName) == 0) {
            index = colIndex;
            break;
        }
        token = strtok(NULL, ",");
        colIndex++;
    }

    if (index == -1) {
        printf("Attribute name '%s' not found in the CSV file.\n", attrName);
        fclose(input);
        fclose(output);
        return;
    }

    // Write the modified attribute names to the output file
    fprintf(output, "%s", strtok(row, "\n"));
    fprintf(output, "\n");

    // Process the remaining rows
    while (fgets(row, sizeof(row), input) != NULL) {
        colIndex = 0;
        token = strtok(row, ",");
        while (token != NULL) {
            if (colIndex != index) {
                fprintf(output, "%s", token);
                if (colIndex != 0) {
                    fprintf(output, ",");
                }
            }
            token = strtok(NULL, ",");
            colIndex++;
        }
        fprintf(output, "\n");
    }

    // Close the input and output files
    fclose(input);
    fclose(output);

    if (remove(filepath) != 0) {
        printf("Failed to remove file '%s'.\n", filename);
        return;
    }
    // Replace the original file with the temporary output file
    if (rename(temppath, filepath) != 0) {
        perror("Error replacing file");
    }
}
```

Fungsi removeColumnFromCSV adalah void function untuk menghapus sebuah index kolom tertentu pada sebuah file csv pada direktory database. removeColumnFromCSV menerima nama tabel dan nama atribut yang ingin didrop kolomnya. Fungsi ini dipanggil saat terdapat command drop collumn.

```
int credExist(char *username, char *password) {
    char filePath[100];
    sprintf(filePath, "%s/users.csv", ACCESS_DB_DIR);

    FILE *file = fopen(filePath, "r");
    if (file != NULL) {
        char line[256];
        char loadedUsername[MAX_USERNAME_LENGTH];
        char loadedPassword[MAX_PASSWORD_LENGTH];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,],%s", loadedUsername, loadedPassword);
            //printf("%s,%s\n", loadedUsername, loadedPassword);
            if (strcmp(loadedUsername, username) == 0 && strcmp(loadedPassword, password) == 0) {
                fclose(file);
                return 1; // Authentication successful
            }
        }

        fclose(file);
    }
    return 0; // Authentication failed
}
```

Fungsi credExist merupakan fungsi return integer untuk mengecek apakah pasangan username dan password terdapat pada table access_database/access.csv, fungsi mereturn 1 jika ditemukan dan return 0 jika tidak.

```
int hasAccessRights(char *username, char *database) {
    if (strcmp(username, "root") == 0)
        return 1; // Root has access to all databases

    char filePath[100];
    sprintf(filePath, "%s/access.csv", ACCESS_DB_DIR);

    FILE *file = fopen(filePath, "r");
    if (file != NULL) {
        char line[256];
        char loadedUsername[MAX_USERNAME_LENGTH];
        char loadedDatabase[MAX_USERNAME_LENGTH];

        while (fgets(line, sizeof(line), file)) {
            sscanf(line, "%[^,],%s", loadedUsername, loadedDatabase);

            if (strcmp(loadedUsername, username) == 0 && strcmp(loadedDatabase, database) == 0) {
                fclose(file);
                return 1; // User has access rights to the database
            }
        }

        fclose(file);
    }

    return 0; // User does not have access rights to the database
}
```

Fungsi hasAccessRights merupakan fungsi dengan return integer untuk mengecek apakah sebuah username memiliki permission untuk menggunakan sebuah database. hasAccessRights mengiterasi setiap baris dari access_database/access.csv untuk mencocokkan username dan nama database yang bersangkutan. Jika baris ditemukan maka akan return 1 dan return 0 jika tidak.

```
int dropDatabase(char *database){
    char dirPath[2048], command[3072];
    sprintf(dirPath, "%s/%s", DATABASE_DIR, database);
    
    sprintf(command, "rm -rf %s", dirPath);
    int status = system(command);
    if (status == 0) {
        printf("Database removed: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to remove database");
        return 0; // Database creation failed
    }
}
```

Fungsi dropDatabase merupakan fungsi dengan return integer untuk menghapus directory database berdasarkan nama directory dari parameter. dropDatabase menggunakan syntax system untuk remove directory database bersangkutan, return 1 jika remove berhasil dan return 0 jika gagal.

```
int createDatabase(char *database) {
    char dirPath[100];
    sprintf(dirPath, "%s/%s", DATABASE_DIR, database);

    int status = mkdir(dirPath, 0777);
    if (status == 0) {
        printf("Database created: %s\n", database);
        return 1; // Database created successfully
    } else {
        perror("Failed to create database");
        return 0; // Database creation failed
    }
}
```

Fungsi createDatabase merupaka fungsi dengan return integer untuk membuat directory database berdasarkan nama dari parameter yang diterima. createDatabase menggunakan syntax mkdir dalam pembuatan directory, return 1 jika mkdir sukses dan return 0 jika gagal.

```
void extractTableAndAttributes(char* inputString, char* tableName, char* attributeString) {
    char inpBuf[1024];
    strcpy(inpBuf, inputString);

    // Find the table name
    char *token = strtok(inpBuf, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, "(");

    strcpy(tableName, token);

    token = strtok(NULL, ")");

    strcpy(attributeString, token);
}

```

Fungsi extractTableAndAttributes merupakan fungsi void untuk memodifikasi string yang diterima pada parameter untuk mengekstrak nama tabel dan nama atribut dari string command raw. extractTable memanfaatkan syntax strtok untuk mendapat bagian nama tabel dan nama-nama atribut. Fungsi ini dipanggil ketika ingin create table.

```
void extractAttributeNames(const char* inputString, char* outputString) {
    char attrBuf[1024];
    strcpy(attrBuf, inputString);

    char* token;
    char* savePtr;
    char* attributeName;

    token = strtok_r(attrBuf, ",", &savePtr);
    while (token != NULL) {
        // Find the first space character to separate attribute name
        attributeName = strtok(token, " ");
        if (attributeName != NULL) {
            strcat(outputString, attributeName);
            strcat(outputString, ",");
        }
        token = strtok_r(NULL, ",", &savePtr);
    }

    // Remove the trailing comma, if any
    if (strlen(outputString) > 0) {
        outputString[strlen(outputString) - 1] = '\0';
    }
    printf("Invoke extract attrnames, results: %s\n", outputString);
}
```

Fungsi extractAttributeNames merupakan fungsi void untuk memodifikasi string attribut sehingga mendapatkan nama-nama atribut tanpa ada tipe datanya. Fungsi ini dipanggil setelah memanggil fungsi extractTableAndAttribute.

```
void removeTable(char *table, char *database){
    char filepath[1024];
    sprintf(filepath, "%s/%s/%s.csv", DATABASE_DIR, database, table);
    printf("imminent remove table: %s\n", filepath);
    if(remove(filepath) == 0) {
        printf("Table '%s' removed successfully.\n", filepath);
    } else {
        perror("Error removing file");
    }
}

```

Fungsi removeTable merupakan fungsi void untuk menghapus file tabel csv dari parameter yang diterima menggunakan syntax remove. Fungsi ini dipanggil ketika command DROP TABLE terpanggil pada fungsi createTable.

```
int createTable(char *table, char *rawAttributes) {
    char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");

    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);
    printf("create table: %s\n", dirPath);

    FILE *tableFile = fopen(dirPath, "a");

    if(tableFile != NULL){
        printf("Table created: %s\n", table);
        char attrNames[1024];
        extractAttributeNames(rawAttributes, attrNames);

        fprintf(tableFile, "%s\n", attrNames);
        fclose(tableFile);
        return 1; // Database created successfully

    } else {
        perror("Failed to create table");
        return 0; // Database creation failed
    }
}
```

Fungsi createTable merupakan fungsi dengan return integer untuk membuat file csv sebagai tabel pada sebuah database. createTabel membuat file csv dengan fopen dengan mode append, setelah fopen berhasil maka akan diappend nama-nama attribut yang didapat setelah memanggil fungsi extractAttribute. return 1 jika berhasil dan return 0 jika gagal.

```
void remove_quotes(char *input_string) {
    char *src = input_string;
    char *dst = input_string;
    while (*src) {
        if (*src != '\'') {
            *dst = *src;
            dst++;
        }
        src++;
    }
    *dst = '\0';
}
```
Fungsi remove_quotes adalah fungsi untuk mengahpus tanda ' pada string
```
int InsertInto(char *table, char *rawAttributes) {
    char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");
    
    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);

    FILE *tableFile = fopen(dirPath, "a");
	printf("%s", dirPath);
    if (tableFile != NULL) {
        printf("Insert Into: %s\n", table);

        remove_quotes(rawAttributes);

        fprintf(tableFile, "%s\n", rawAttributes);
        fclose(tableFile);
        return 1; 

    } else {
        perror("Failed to Insert Into");
        return 0; 
    }
}
```
Fungsi insert into adalah fungsi untuk menulis baris baru ke suatu file csv. dengan cara pertama open ke path file csv table yang diinginkan dengan mode append lalu write atributnya.
```
void getTableNames(char *command, char *table){
	char inpBuf[1024];
    strcpy(inpBuf, command);

    // Find the table name
    char *token = strtok(inpBuf, " ");
    token = strtok(NULL, " ");
    token = strtok(NULL, ";");
    strcpy(table, token);
}
```
Fungsi getTableNames adalah fungsi mendapatkan nama table dari command delete dengan menggunakan strtok.
```
void deleteData(char *table){
	char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");
    
    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);
	
	FILE *file = fopen(dirPath, "r+");
    if (file == NULL) {
        printf("Gagal membuka file %s.\n", tableBuf);
        return;
    }

    // Membaca baris pertama dan menyimpannya dalam string
    char line[256];
    fgets(line, sizeof(line), file);

    // Mengosongkan file dengan mengganti isinya dengan baris pertama
    freopen(NULL, "w", file);
    fputs(line, file);

    fclose(file);
    printf("Berhasil menghapus semua isi kecuali baris pertama dari file %s.\n", tableBuf);
}
```
Fungsi deleteData adalah fungsi untuk menghapus semua baris pada file csv kecuali baris pertama, caranya adalah baca baris pertama lalu reopen dengan write lalu overwrite baris pertama tersebut.
```
void getUpdateData(char* inputString, char* tableName, char* kolom, char* value){
	char inpBuf[1024];
    strcpy(inpBuf, inputString);

    // Find the table name
    char *token = strtok(inpBuf, " ");
    token = strtok(NULL, " ");
    strcpy(tableName, token);
    // Find the column name
    token = strtok(NULL, " ");
    token = strtok(NULL, " ");
    removeSpaces(token);
    removeNewlines(token);
    strcpy(kolom, token);
    // Find the value
    token = strtok(NULL, "=");
    token = strtok(token, ";");
    remove_quotes(token);
    strcpy(value, token);
}
```
Fungsi getUpdateData adalah fungsi untuk mendpatkan nama tabel, kolom, value dari command update, menggunakan strtok yang memecah string satu persatu.
```
void Update(char *table, char* kolom, char* value){
	char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");
    
    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);
    
    FILE *file = fopen(dirPath, "r+");
    if (file == NULL) {
        printf("Gagal membuka file %s\n", dirPath);
        return;
    }

    char line[1024];
    fgets(line, sizeof(line), file); // Membaca baris pertama dan menyimpannya
	
	int column_counter = 1;
	int cek=0;
    // Mencari kolom dengan kata kunci
    char *token2;
    strcpy(token2,line);
	

	if(strcmp(strtok(token2, ","), kolom) == 0){
		
	}else{
		while(token2!=NULL){
			if (strstr(token2, kolom) != NULL){
	            cek = 1;
	            break;
	        }
	        token2 = strtok(NULL, ",");
	        
	        column_counter++;
		}
		if(cek==0) column_counter++;
	}


    printf("Kata kunci '%s' ditemukan pada kolom ke-%d.\n", kolom, column_counter);
    
	int col = column_counter-1;


    while (fgets(line, sizeof(line), file)) {
        char* token;
        char* columns[MAX_COLUMNS];
        int currentCol = 0;

        token = strtok(line, ",");
        while (token != NULL && currentCol < MAX_COLUMNS) {
            columns[currentCol++] = token;
            token = strtok(NULL, ",");
        }

        if (col >= 0 && col < currentCol) {
            strcpy(columns[col], value);
        } else {
            printf("Kolom tidak valid.\n");
            fclose(file);
            return;
        }

        fseek(file, -strlen(line), SEEK_CUR);
        for (int i = 0; i < currentCol; i++) {
            fputs(columns[i], file);
            if (i < currentCol - 1) {
                fputs(",", file);
            }
        }
        fputc('\n', file);
        fflush(file);
    }


	
    fclose(file);
    printf("Berhasil mengganti semua nilai kolom %s pada tabel %s menjadi 'api'.\n", kolom, tableBuf);
}
```
Fungsi update adalah fungsi untuk mengupdate data pada kolom tertentu.

```
void updateLog(char *user, char *command){
    FILE *logDB;
    char logpath[256] = {0};
    strcpy(logpath, LOG_DIR);

    time_t rawTime;
    struct tm* timeInfor;
    char timeBuffer[256] = {0};
    char commandBuffer[256];

    char *token = strtok(command, ";");
    if (token != NULL) {
        strcpy(commandBuffer, token);
    }

    time(&rawTime);
    timeInfor = localtime(&rawTime);
    strftime(timeBuffer, sizeof(timeBuffer), "%Y-%m-%d %X", timeInfor);

    logDB = fopen(logpath, "a");
    if (logDB != NULL) {
        fprintf(logDB, "%s:%s:%s\n", timeBuffer, user, command);
        fclose(logDB);
    } else {
        perror("Failed to open log file");
    }
}
```

Fungsi updateLog() digunakan untuk menambahkan log dari command yang diinput oleh user. Fungsi ini akan melakukan append dari file dblog.txt yang terletak di database/dblog.txt dengan menggunakan format timestamp(yyyy-mm-dd hh:mm:ss):username:command. 


```
void parseUpdateQuery(const char* query, char* table, char* kolom, char* value, char* wkolom, char* wvalue) {
    char temp[100];
    strncpy(temp, query, sizeof(temp));
    temp[sizeof(temp) - 1] = '\0';

    char* token = strtok(temp, " ");
    while (token != NULL) {
        if (strcmp(token, "UPDATE") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "UPDATE"
            if (token != NULL) {
                strncpy(table, token, sizeof(table));
                table[sizeof(table) - 1] = '\0';
            }
        } else if (strcmp(token, "SET") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "SET"
            if (token != NULL) {
                strncpy(kolom, token, sizeof(kolom));
                kolom[sizeof(kolom) - 1] = '\0';

                token = strtok(NULL, " "); // Mengabaikan tanda sama (=)
                if (token != NULL) {
                    token = strtok(NULL, "'");
                    if (token != NULL) {
                        strncpy(value, token, sizeof(value));
                        value[sizeof(value) - 1] = '\0';
                    }
                }
            }
        } else if (strcmp(token, "WHERE") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "WHERE"
            if (token != NULL) {
                strncpy(wkolom, token, sizeof(wkolom));
                wkolom[sizeof(wkolom) - 1] = '\0';

                token = strtok(NULL, "=");
                token = strtok(token, " ");
                remove_quotes(token);
                token = strtok(token, ";");
                strcpy(wvalue,token);
            }
        }

        token = strtok(NULL, " ");
    }
}
```
Fungsi parseUpdateQuery adalah fungsi untuk mengambil nama tabel, kolom, value, where kolom, dan where value dari command update where menggunakan strtok.
```
void updateWhere(const char* table, const char* column, const char* value, const char* updateColumn, const char* updateValue) {
	char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");
    
    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);
	
    FILE* file = fopen(dirPath, "r+");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file); // Membaca baris pertama (header) dan mengabaikannya

    int columnIdx = -1;
    char* token = strtok(line, ",");
    int col = 0;
    while (token != NULL && col < MAX_COLUMNS) {
        if (strcmp(token, column) == 0) {
            columnIdx = col;
            break;
        }
        token = strtok(NULL, ",");
        col++;
    }

    if (columnIdx == -1) {
        printf("Kolom tidak ditemukan.\n");
        fclose(file);
        return;
    }

    while (fgets(line, sizeof(line), file)) {
        char* columns[MAX_COLUMNS];
        int currentCol = 0;

        token = strtok(line, ",");
        while (token != NULL && currentCol < MAX_COLUMNS) {
            columns[currentCol++] = token;
            token = strtok(NULL, ",");
        }

        if (currentCol > columnIdx && strcmp(columns[columnIdx], value) == 0) {
            strcpy(columns[columnIdx], updateValue);
        }

        fseek(file, -strlen(line), SEEK_CUR);
        for (int i = 0; i < currentCol; i++) {
            fputs(columns[i], file);
            if (i < currentCol - 1) {
                fputs(",", file);
            }
        }
        fputc('\n', file);
        fflush(file);
    }

    fclose(file);
}
```
fungsi updateWhere adalah fungsi mengupdate kolom menjadi value baru sesuai dengan where statement.
```
void parseDeleteQuery(const char* query, char* table, char* kolom, char* value) {
    char temp[100];
    strncpy(temp, query, sizeof(temp));
    temp[sizeof(temp) - 1] = '\0';

    char* token = strtok(temp, " ");
    while (token != NULL) {
        if (strcmp(token, "DELETE") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "DELETE"
            if (token != NULL && strcmp(token, "FROM") == 0) {
                token = strtok(NULL, " "); // Mengabaikan kata "FROM"
                if (token != NULL) {
                    strncpy(table, token, sizeof(table));
                    table[sizeof(table) - 1] = '\0';
                }
            }
        } else if (strcmp(token, "WHERE") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "WHERE"
            if (token != NULL) {
                strncpy(kolom, token, sizeof(kolom));
                kolom[sizeof(kolom) - 1] = '\0';
				
                token = strtok(NULL, "=");
                token = strtok(token, " ");
                remove_quotes(token);
                token = strtok(token, ";");
                strcpy(value,token);
            }
        }

        token = strtok(NULL, " ");
    }
}
```
Fungsi parseDeleteQuery adalah fungsi untuk mengambil nama tabel, kolom, value  dari command delete where menggunakan strtok.
```
void deleteWhere(const char* table, const char* column, const char* value) {
	char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");
    
    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);
	
    FILE* file = fopen(dirPath, "r+");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file); // Membaca baris pertama (header) dan mengabaikannya

    int columnIdx = -1;
    char* token = strtok(line, ",");
    int col = 0;
    while (token != NULL && col < MAX_COLUMNS) {
        if (strcmp(token, column) == 0) {
            columnIdx = col;
            break;
        }
        token = strtok(NULL, ",");
        col++;
    }

    if (columnIdx == -1) {
        printf("Kolom tidak ditemukan.\n");
        fclose(file);
        return;
    }

    FILE* tempFile = fopen("temp.csv", "w");
    if (tempFile == NULL) {
        printf("File temp.csv tidak dapat dibuat.\n");
        fclose(file);
        return;
    }

    fputs(line, tempFile); // Menulis kembali baris pertama (header)

    while (fgets(line, sizeof(line), file)) {
        char* columns[MAX_COLUMNS];
        int currentCol = 0;

        token = strtok(line, ",");
        while (token != NULL && currentCol < MAX_COLUMNS) {
            columns[currentCol++] = token;
            token = strtok(NULL, ",");
        }

        if (currentCol > columnIdx && strcmp(columns[columnIdx], value) != 0) {
            for (int i = 0; i < currentCol; i++) {
                fputs(columns[i], tempFile);
                if (i < currentCol - 1) {
                    fputs(",", tempFile);
                }
            }
            fputc('\n', tempFile);
        }
    }

    fclose(file);
    fclose(tempFile);

    remove(table);
    rename("temp.csv", table);
}
```
Fungsi deleteWhere adalah fungsi untuk menghapus data sesuai dengan where statement.
```
void parseSelectQuery(const char* query, char* table, char* kolom, char* value) {
    char temp[100];
    strncpy(temp, query, sizeof(temp));
    temp[sizeof(temp) - 1] = '\0';

    char* token = strtok(temp, " ");
    while (token != NULL) {
        if (strcmp(token, "SELECT") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "SELECT"
            token = strtok(NULL, " ");
            token = strtok(NULL, " ");
            strcpy(table,token);
        } else if (strcmp(token, "WHERE") == 0) {
            token = strtok(NULL, " "); // Mengabaikan kata "WHERE"
            if (token != NULL) {
                strncpy(kolom, token, sizeof(kolom));
                kolom[sizeof(kolom) - 1] = '\0';

                token = strtok(NULL, "=");
                token = strtok(token, " ");
                remove_quotes(token);
                token = strtok(token, ";");
                strcpy(value,token);
            }
        }

        token = strtok(NULL, " ");
    }
}
```
Fungsi parseSelectQuery adalah fungsi untuk mengambil nama tabel, kolom, value  dari command delete where menggunakan strtok.
```
void selectWhere(const char* table, const char* column, const char* value) {
	char dirPath[3072];
    char fixedTable[1024];
    strcpy(fixedTable, table);
    char *tableBuf = strtok(fixedTable, " ");
    
    sprintf(dirPath, "%s/%s/%s.csv", DATABASE_DIR, CURDATABASE, tableBuf);
	
    FILE* file = fopen(dirPath, "r");
    if (file == NULL) {
        printf("File tidak dapat dibuka.\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file); // Membaca baris pertama (header) dan mengabaikannya

    int columnIdx = -1;
    char* token = strtok(line, ",");
    int col = 0;
    while (token != NULL && col < MAX_COLUMNS) {
        if (strcmp(token, column) == 0) {
            columnIdx = col;
            break;
        }
        token = strtok(NULL, ",");
        col++;
    }

    if (columnIdx == -1) {
        printf("Kolom tidak ditemukan.\n");
        fclose(file);
        return;
    }

    while (fgets(line, sizeof(line), file)) {
        char* columns[MAX_COLUMNS];
        int currentCol = 0;

        token = strtok(line, ",");
        while (token != NULL && currentCol < MAX_COLUMNS) {
            columns[currentCol++] = token;
            token = strtok(NULL, ",");
        }

        if (currentCol > columnIdx && strcmp(columns[columnIdx], value) == 0) {
            for (int i = 0; i < currentCol; i++) {
                printf("%s ", columns[i]);
            }
            printf("\n");
        }
    }

    fclose(file);
}
```
Fungsi select adalah fungsi untuk mengprint data sesuai dengan where statement.


## fungsi main

```
// Create databases directory if it doesn't exist
    struct stat st;

    if (stat("./database", &st) != 0 || !S_ISDIR(st.st_mode)) {
        int status = mkdir("./database/", 0777);
        if (status != 0) {
            perror("Failed to create database directory");
            exit(EXIT_FAILURE);
        }
    }

    if (stat(DATABASE_DIR, &st) != 0 || !S_ISDIR(st.st_mode)) {
        int status = mkdir(DATABASE_DIR, 0777);
        if (status != 0) {
            perror("Failed to create databases directory");
            exit(EXIT_FAILURE);
        }
    }

    // Create access_database directory if it doesn't exist
    if (stat(ACCESS_DB_DIR, &st) != 0 || !S_ISDIR(st.st_mode)) {
        int status = mkdir(ACCESS_DB_DIR, 0700);
        if (status != 0) {
            perror("Failed to create access_database directory");
            exit(EXIT_FAILURE);
        }
    }

    // Create user table if it doesn't exist
    char userTablePath[100];
    sprintf(userTablePath, "%s/users.csv", ACCESS_DB_DIR);

    FILE *userTableFile = fopen(userTablePath, "a");
    if (userTableFile == NULL) {
        perror("Failed to create user table");
        exit(EXIT_FAILURE);
    }
    fclose(userTableFile);

    // Create access table if it doesn't exist
    char accessTablePath[100];
    sprintf(accessTablePath, "%s/access.csv", ACCESS_DB_DIR);

    FILE *accessTableFile = fopen(accessTablePath, "a");
    if (accessTableFile == NULL) {
        perror("Failed to create access table");
        exit(EXIT_FAILURE);
    }
    fclose(accessTableFile);

    // Create server socket
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024];

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    /*
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }*/

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server started\n");

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
        perror("accept failed");
        exit(EXIT_FAILURE);
    }

    char username[1024], password[1024], message[5000]={0};

    // startdaemon();

    while (1){
        memset(buffer, 0, sizeof(buffer));
        valread = read(new_socket, buffer, sizeof(buffer) - 1);
        memset(message, 0, sizeof(message));

        char command[1024], bufcmd[1024];
        strcpy(command, buffer);
        strcpy(bufcmd, buffer);

```

Berikut merupakan fungsi main dari database.c, pada database.c akan dicek terlebih dahulu apakah sudah ada directory ./database , ./database/databases , dan ./database/databases/access_database. Jika belum maka akan mkdir terlebih dahulu. Setelah itu dicek apakah pada ./database/databases/access_database sudah terdapat tabel users.csv dan access.csv, jika belum maka touch dengan fopen kedua file tersebut. Setelah directory-directory dan file basic telah terbuat maka dilanjutkan dengan setting socket. Setelah socket berhasil terbuat dan menerima koneksi dari program client maka dilanjutkan dengan loop hiwle true untuk menerima dan membaca message dari program client.

```
if(strstr(command, "launcher") != NULL){ // DONE
            strcpy(message, "Login credentials detected");
            char *tokBuf = strtok(bufcmd, " ");
            tokBuf = strtok(NULL, " ");
            strcpy(username, tokBuf);
            tokBuf = strtok(NULL, " ");
            strcpy(password, tokBuf);

            //char message[5000];
            //sprintf(message, "username: %s\n password: %s\n", username, password);
            if(strcmp(username, "root") == 0|| credExist(username, password) == 1){
                printf("Credentials authenticated, welcome %s!\n", username);
            }else{
                printf("Credentials invalid!\n"); 
            }
        }
```

Jika command yang diterima adalah launcher maka akan diambil username dan password pada command tersebut menggunakan strtok. Jika username adalah root atau credExist untuk username dan password yang diterima bernilai 1 maka autentikasi credentials berhasil, jika tidak maka gagal.

```
else if(strstr(command, "CREATE USER") != NULL){ // DONE
            updateLog(username, command);
            if(strcmp(username, "root")==0){
                char regUser[1024], regPass[1024];

                char *tokBuf = strtok(bufcmd, " ");
                tokBuf = strtok(NULL, " ");

                // Extract the nama_user
                tokBuf = strtok(NULL, " ");
                strcpy(regUser, tokBuf);
                if (tokBuf != NULL) {
                    printf("inserted Username: %s\n", regUser);
                }

                // Skip the "IDENTIFIED" and "BY" tokens
                tokBuf = strtok(NULL, " ");
                tokBuf = strtok(NULL, " ");

                // Extract the password_user
                tokBuf = strtok(NULL, ";");

                strcpy(regPass, tokBuf);
                if (tokBuf != NULL) {
                    printf("inserted Password: %s\n", regPass);
                }

                if(userExist(regUser)){
                    printf("This username is already exist!\n");
                    strcpy(message, "This username is already exist");
                }else{
                    createUser(regUser, regPass);
                    strcpy(message, "Create username success");
                }
            }else{
                printf("Invalid syntax\n");
                strcpy(message, "No authorities to create user!");
            }
        }

```

Jika command yang diterima adalah CREATE USER maka akan dipanggil fungsi updateLog dan cek username apakah root. Jika username adalah root maka dilanjutkan ambil username dan password yang akan didaftarkan menggunakan strtok. Selanjutnya dicek apakah username sudah terdaftar, jika sudah maka return message "Username already exist" dan jika tidak maka akan dipanggil fungsi createUser.

```
else if(strstr(command, "USE") != NULL){
	updateLog(username, command);
        char *tokBuf = strtok(bufcmd, " ");
        tokBuf = strtok(NULL, " ");
        char *database = strtok(tokBuf, ";");

        if(hasAccessRights(username, database) == 1){
        	strcpy(CURDATABASE, database);
        	printf("Database Accessed\n");
               	strcpy(message, "Database accessed");
	}else{
		//tidak punya
		printf("Tidak ada database yang dituju\n");
                strcpy(message, "No authorization upon database!");
	}
}

```

Jika command yang diterima adalah USE maka akan dipanggil fugnsi updateLog. Selanjutnya diambil nama database menggunakan strtok dan dipanggil fungsi hasAccessRights untuk username dan nama database yang ingin di-use. Jika hasAccessRights return 1 maka nama database dicopy menuju string CURDATABASE global dan jika return 0 maka akan return message "No authorization upon database".

```
else if(strstr(command, "CREATE DATABASE") != NULL){ // DONE
	updateLog(username, command);
        char *tokBuf = strtok(bufcmd, " ");
       	tokBuf = strtok(NULL, " ");
        tokBuf = strtok(NULL, " ");
        char *database = strtok(tokBuf, ";");
        database[strlen(database)] = '\0';
       	createDatabase(database);
        strcpy(message, "Invoke create database");

        if(strcmp(username, "root") != 0){grantAccess(username, database);} // jika pembuat bukan root
        grantAccess("root", database);

}
```

Jika command yang diterima adalah CREATE DATABASE maka panggil fungsi updateLog diikuti dengan ambil nama database dari command dan pemanggilan fungsi createDatabase.

```
else if(strstr(command, "GRANT PERMISSION")){ //DONE
            updateLog(username, command);
            if(strcmp(username, "root") == 0){   
                char *tokBuf = strtok(bufcmd, " ");
                tokBuf = strtok(NULL, " ");
                char *dbAcc = strtok(NULL, " ");
                tokBuf = strtok(NULL, " ");
                char *userAcc = strtok(NULL, ";");

                if(userExist(userAcc)){
                    grantAccess(userAcc, dbAcc);
                    strcpy(message, "Invoke grant access");
                }else{
                    printf("user is not exist\n");
                    strcpy(message, "User is not exist!");
                }
            }else{
                strcpy(message, "No authorization to grant permission!");
            }
        }
```

Jika command yang diterima adalah GRANT PERMISSION maka panggil fungsi updateLog diikuti dengan pengecekan apakah user merupakan root, jika user adalah root maka dilanjutkan dengan ambil username dan password dari command menggunakan strtok. Setelah didapat string username dan password maka dicek apakah username yang akan didaftarkan sudah terdaftar menggunakan fungsi userExist, jika return 1 maka lanjutkan dengan pemanggilan fungsi grantAccess.

```
else if(strstr(command, "CREATE TABLE") != NULL){ // DONE
            updateLog(username, command);
            if(strcmp(CURDATABASE, "") != 0){
                char tableName[1024], attr[1024];
                extractTableAndAttributes(command, tableName, attr);
                printf("Tabel name: %s\nattributes: %s\n", tableName, attr);
                createTable(tableName, attr);
                strcpy(message, "Invoke create table");
            }
            else{
                printf("Use a database first to create table\n");
                strcpy(message, "Use a databse first!");
            }
}

```

Jika command yang diterima adalah CREATE TABLE maka panggil fungsi updateLog diikuti dengan pengecekan CURDATABASE, jika CURDATABASE tidak kosong maka dilanjutkan dengan ekstrak nama tabel dan rawAttributes dengan pemanggilan fungsi extractTableAndAttributes dan createTable.

```
else if(strstr(command, "DROP") != NULL){
            updateLog(username, command);
            if(strstr(command, "DATABASE") != NULL){
                char dropDB[1024];
                char *tokBuf = strtok(bufcmd, " ");
                tokBuf = strtok(NULL, " ");

                // Extract the nama database
                tokBuf = strtok(NULL, ";");
                strcpy(dropDB, tokBuf);
                if (tokBuf != NULL) {
                    printf("inserted drop database: %s\n", dropDB);
                }

                if(hasAccessRights(username, dropDB) == 1){
                    dropDatabase(dropDB);
                    rmAccDropDB(dropDB);
                    strcpy(message, "Invoke drop database");  
                }else{
                    printf("%s has no right upon this %s database\n", username, dropDB);
                    strcpy(message, "No authorization upon database!");
                }
            }
            else if(strstr(command, "TABLE") != NULL){
                if(strcmp(CURDATABASE, "") != 0){
                    char dropTB[1024];
                    char *tokBuf = strtok(bufcmd, " ");
                    tokBuf = strtok(NULL, " ");

                    // Extract the nama database
                    tokBuf = strtok(NULL, ";");
                    strcpy(dropTB, tokBuf);
                    if (tokBuf != NULL) {
                        printf("inserted drop table: %s\n", dropTB);
                    }

                    removeTable(dropTB, CURDATABASE);
                    strcpy(message, "Invoke drop table");       
                }else{
                    printf("Use a database first!\n");
                    strcpy(message, "Use a databse first!");
                }
            }else if(strstr(command, "COLUMN") != NULL){
                if(strcmp(CURDATABASE, "") != 0){
                    char dropCol[1024], sourceTB[1024];
                    char *tokBuf = strtok(bufcmd, " ");
                    tokBuf = strtok(NULL, " ");

                    // Extract the nama database
                    tokBuf = strtok(NULL, " ");
                    strcpy(dropCol, tokBuf);
                    tokBuf = strtok(NULL, " ");
                    tokBuf = strtok(NULL, ";");
                    strcpy(sourceTB, tokBuf);

                    printf("inserted drop Column: %s from table: %s\n", dropCol, sourceTB);
                    removeColumnFromCSV(sourceTB, dropCol);
                    strcpy(message, "Invoke drop column");  
                }else{
                    printf("Use a database first!\n");
                    strcpy(message, "Use a database first!");
                }
            }
            else{printf("Invalid DROP command\n");}
        }
```

Jika command yang diterima adalah DROP maka akan updateLog dan dilakukan double layer check untuk menggolongkan command DROP yang dimaksudkan. Jika DROP DATABASE maka ambil nama database yang akan didrop dilanjutkan dengan pengecekan apakah user memiliki permission terhadap database bersangkutan dengan pemanggilan fungsi hasAccessRights. Jika hasAccessRight return 1 maka lanjutkan dengan pemanggilan fungsi dropDatabase dan rmAccDropDB. Jika DROP TABLE maka dicek terlebih dahulu apakah CURDATABASE tidak kosong, jika CURDATABASE tidak kosong maka dilanjutkan dengan ambil nama tabel dari command dengan strtok dilanjutkan dengan pemanggilan fungsi removeTable. Jika DROP COLUMN maka dicek terlebih dahulu apakah CURDATABASE tidak kosong, jika CURDATABASE tidak kosong maka dilanjutkan dengan ambil nama kolom dan tabel dari command dengan strtok dilanjutkan dengan pemanggilan removeColumnFromCSV.


Jika command mengandunng kata INSERT INTO maka pertama lakukan updateLog lalu cek apakah sekarang memiliki current database. Jika ada, selanjutnya deklarasi 2 variabel untuk menyimpan nama tabel dan atribut tabel, setelah itu menggunakan fungsi extractTableAndAttributes untuk mendatpakan nama tabel dan atribut setelah itu untuk menulis ke file csv gunakan fungsi InsertInto dengan memasukkan parameter nama tabel dan atributnya.
```
else if(strstr(command, "INSERT INTO") != NULL){
            updateLog(username, command);
			if(strcmp(CURDATABASE, "") != 0){
                char tableName[1024], attr[1024];
                extractTableAndAttributes(command, tableName, attr);
                remove_quotes(attr);
                printf("Insert Into\nTabel name: %s\nValues: %s\n", tableName, attr);
                InsertInto(tableName, attr);
                strcpy(message, "Invoke insert command");  
            }
            else{
                printf("Use a database first to create table\n");
                strcpy(message, "Use a database first!");
            }
		}
```
Jika command mengandung kata DELETE FROM dan tidak mengangdung WHERE maka pertama lakukan updatelog terlebih dahulu lalu cek apakah current database ada. Jika ada, selanjutnya deklarasi variabel untuk menyimpan nama tabel, lalu menggunakan fungsi getTableNames untuk mendapatkan nama tabel, setelah itu menggunakan fungsi deleteData untuk melakukan delete pada tabel.
```
		else if(strstr(command, "DELETE FROM") != NULL && strstr(command, "WHERE") == NULL){
            updateLog(username, command);
            if(strcmp(CURDATABASE, "") != 0){
                char tableName[1024];
                
                getTableNames(command, tableName);
                printf("Delete data from Tabel name: %s\n", tableName);
                deleteData(tableName);
                strcpy(message, "Invoke delete command"); 
            }
            else{
                printf("Use a database first to create table\n");
                strcpy(message, "Use a database first!");
            }
		}
```
Jika command mengandung kata update tanpa where maka pertama lakukan updatelog lalu cek apakah ada current Database, jika ada selanjutnya adalah deklarasi 3 variabel untuk menyimpan nama tabel, kolom, dan value, lalu gunakan fungsi getUpdateData untuk mendapatkan ketiga variabel tersebut lalu untuk melakukan update gunakan fungsi Update.
```
        else if(strstr(command, "UPDATE") != NULL && strstr(command, "WHERE") == NULL){
			updateLog(username, command);
	    	if(strcmp(CURDATABASE, "") != 0){
	            char tableName[1024], kolom[1024], value[1024];
				getUpdateData(command, tableName, kolom, value);
	
	    		printf("Update data from : %s\nkolom : %s\nvalue : %s\n", tableName, kolom, value);
				Update(tableName, kolom, value);
	        }
	        else{
	            printf("Use a database first to create table\n");
	            strcpy(message, "Use a databse first!");
	        }
		}
```
Jika command mengangdung kata delete from dan ada where nya maka lakukan updatelog, lalu cek current database, lalu deklarasi 3 variabel, lalu gunakan fungsi parseDeleteQuery untuk mendapatkan value dari 3 variabel tersebut, setelah itu gunakan fungsi deleteWhere untuk melakukan delete sesuai dengan where statement. 
```
        else if(strstr(command, "DELETE FROM") != NULL){
			updateLog(username, command);
	    	if(strcmp(CURDATABASE, "") != 0){
	            char table[1024];
			    char kolom[1024];
			    char value[1024];
			    
			    parseDeleteQuery(command, table, kolom, value);
			    printf("Delete\nTabel: %s\n", table);
			    printf("Kolom: %s\n", kolom);
			    printf("Value: %s\n", value);
			    
			    deleteWhere(table, kolom, value);
	        }
	        else{
	            printf("Use a database first to create table\n");
	            strcpy(message, "Use a databse first!");
	        }
		}
```
Jika command mengangdung kata update dan ada where nya maka lakukan updatelog, lalu cek current database, lalu deklarasi 5 variabel, lalu gunakan fungsi parseSelectQuery untuk mendapatkan value dari 5 variabel tersebut, setelah itu gunakan fungsi updateWhere untuk melakukan update sesuai dengan where statement. 
```
        else if(strstr(command, "UPDATE") != NULL){
			updateLog(username, command);
	    	if(strcmp(CURDATABASE, "") != 0){
	            char table[1024];
			    char kolom[1024];
			    char value[1024];
			    char wkolom[1024];
			    char wvalue[1024];
			    
			    parseUpdateQuery(command, table, kolom, value, wkolom, wvalue);
			    printf("UPDATE\nTabel: %s\n", table);
			    printf("Kolom: %s\n", kolom);
			    printf("Value: %s\n", value);
			    printf("Where kolom: %s\n", wkolom);
			    printf("Where value: %s\n", wvalue);
			    
    			updateWhere(table, kolom, value, wkolom, wvalue);
	        }
	        else{
	            printf("Use a database first to create table\n");
	            strcpy(message, "Use a databse first!");
	        }
		}
```
Jika command mengangdung kata select from dan ada where nya maka lakukan updatelog, lalu cek current database, lalu deklarasi 3 variabel, lalu gunakan fungsi parseSelectQuery untuk mendapatkan value dari 3 variabel tersebut, setelah itu gunakan fungsi selectWhere untuk melakukan delete sesuai dengan where statement. 
```
        else if(strstr(command, "SELECT") != NULL){
			updateLog(username, command);
	    	if(strcmp(CURDATABASE, "") != 0){
	    		char table[100];
			    char kolom[100];
			    char value[100];
			
			    parseSelectQuery(command, table, kolom, value);
			    printf("Select\nTabel: %s\n", table);
			    printf("Kolom: %s\n", kolom);
			    printf("Value: %s\n", value);
	    		
	            selectWhere(table, kolom, value);
	        }
	        else{
	            printf("Use a database first to create table\n");
	            strcpy(message, "Use a databse first!");
	        }
		}
        else{
            strcpy(message, "Unknown/invalid syntax!");
        }

```

